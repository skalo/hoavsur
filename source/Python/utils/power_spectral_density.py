import librosa
import numpy as np
import matplotlib.pyplot as plt
from pylab import rcParams
from scipy import signal
from scipy.io import wavfile
from scipy.integrate import simps

def psd(geophony, biophony, anthropophony, output_path, show=False):

    sr = 24050
    g, _ = librosa.load(geophony, sr=sr)
    b, _ = librosa.load(biophony, sr=sr)
    a, _ = librosa.load(anthropophony, sr=sr)

    '''
    # using scipy instead
    sr = 48000
    nbits = 16
    _, g = wavfile.read(geophony)
    g =  np.divide(g, 2 ** (nbits -1))[...,1]
    _, b = wavfile.read(geophony)
    b =  np.divide(b, 2 ** (nbits -1))[...,1]
    _, a = wavfile.read(geophony)
    a =  np.divide(a, 2 ** (nbits -1))[...,1]
    '''

    gf, Pxx_denG = signal.welch(g, sr, nperseg=1024)
    bf, Pxx_denB = signal.welch(b, sr, nperseg=1024)
    af, Pxx_denA = signal.welch(a, sr, nperseg=1024)

    rcParams['figure.figsize'] = 16, 9
    rcParams["figure.dpi"] = 300
    plt.figure()
    plt.title('Power Spectral Density')
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD')
    plt.semilogy(gf, Pxx_denG, label="Geophony")
    plt.semilogy(bf, Pxx_denB, label="Biophony")
    plt.semilogy(af, Pxx_denA, label="Anthropophony")
    plt.legend(loc='best')
    plt.tight_layout()
    plt.savefig(output_path)
    if show:
        plt.show()
    plt.close()
    # Speculative initial condition for the population model, based on PSD:
    # Area under the curve
    areaG = simps(Pxx_denG, dx=1)
    areaB = simps(Pxx_denB, dx=1)
    areaA = simps(Pxx_denA, dx=1)

    return [areaG, areaB, areaA]
