import numpy as np
from sklearn.preprocessing import MinMaxScaler

def initc(areas):
    areaG, areaB, areaA = areas
    scaler = MinMaxScaler((1, 10))
    data = np.array([areaG, areaB, areaA]).reshape(-1, 1)
    scaled = scaler.fit_transform(data)
    c0 = [int(np.ceil(val)) for val in scaled]

    return c0
