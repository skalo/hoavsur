# High-Order Surrogacy for the Audiovisual Display of Dance

Accompanying repository for the paper:

  Stefano Kalonaris and Iannis Zannos (2021). "High-Order Surrogacy for the Audiovisual DIsplay of Dance". In *Proceedings of the 26th International on Auditory Display* (ICAD). Online.



## Contents

* ``./data``
    - ``audio``
    - ``mocap``
    - ``video``
* ``./output``
    - ``plots``
    - ``sonification``
    - ``video_frames``
    - ``video_render``
* ``./source``
    - ``Python``
    - ``SC``



## Notes:

The mini-dance files relative to Andria Michaelidou's performance (``Andria_Afraid_v1.bvh`` and ``Andria_Afraid_v1.mp4``) were downloaded from the [DanceDB](http:/dancedb.eu) project.


The ``.csv`` files in ``./data`` were obtained parsing ``Andria_Afraid_v1.bvh`` with [pymo](https://github.com/omimo/PyMO/) and processing the dataframe with ``pandas``, ``scipy`` and ``scikit-learn`` by means of *smoothing*, *feature selection* and *feature projection* (principal component analysis). Scripts for this pre-processing are not included in this repository. The release of a separate Python package for movement analysis is planned, to this end.

There are further dependencies in order to run the Python scripts, the most important being [openCV](https://pypi.org/project/opencv-python/).

It is recommended to make a dedicated environment, with the method of your choice (virtualenv, pipenv, conda, etc.), so to take care of all the dependencies.

All outputs are provided, with the exception of the rendered audiovisual object resulting from the juxtaposition of the sonification and the processed video files (you can do this yourself however you prefer). But feel free to re-run the following:

* ``./source/Python/mutualism.py``
* ``./source/Python/video_processing.py``
* ``./source/SC/sonification.scd`` (uncomment where necessary to record)

The final audiovisual object can be accessed [here](https://archive.org/details/2021-kalonaris-audiovisual-surrogacy-3).
