from matplotlib import pyplot as plt
from matplotlib.ticker import FuncFormatter
from pylab import rcParams
import numpy as np


class PopModel():
	def __init__(self, c0, growths, interactions):
		self._c0 = c0
		self._growths = growths
		self._interactions = interactions

	def simulate(self,steps, dt):
		def _deltapops(populations, growths, interactions):
			p1, p2, p3 = populations
			g1, g2, g3 = growths
			i11, i12, i13 = interactions[0]
			i21, i22, i23 = interactions[1]
			i31, i32, i33 = interactions[2]
			p1dt = (g1*p1) + (i11*(p1**2)) + (i12*p1*p2) + (i13*p1*p3)
			p2dt = (g2*p2) + (i22*(p2**2)) + (i21*p2*p1) + (i23*p2*p3)
			p3dt = (g3*p3) + (i33*(p3**2)) +(i31*p3*p1) + (i32*p3*p2)
			return np.array([p1dt, p2dt, p3dt])
		pops = np.empty((3,steps)) # empty array
		pops[:, 0] = self._c0
		growths = self._growths
		interactions = self._interactions
		for t_ in range(1, steps):
			pops[:, t_] = pops[:, t_-1] + _deltapops(pops[:, t_-1], growths, interactions) * dt

		return pops

	def plotSimulation(self, pops, dt, save_path, show=False):

		rcParams['figure.figsize'] = 16, 9
		rcParams["figure.dpi"] = 300
		plt.gca().xaxis.grid(True)
		plt.gca().yaxis.grid(True)

		plt.gca().get_xaxis().set_major_formatter(FuncFormatter(lambda x, p: format(int(x * dt), ',')))

		plt.xlabel("Time (secs)")
		plt.ylabel("Population")

		plt.plot(pops[0], label="Geophony")
		plt.plot(pops[1], label="Biophony")
		plt.plot(pops[2], label="Anthropophony")
		plt.legend(loc='best')
		plt.savefig(save_path)
		if show:
			plt.show()
		plt.close()
