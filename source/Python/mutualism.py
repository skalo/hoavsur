import numpy as np
from utils.power_spectral_density import psd
from utils.population_model import PopModel as pop
from utils.initial_condition import initc
from sklearn.preprocessing import normalize

geophony = "../../data/audio/132736_ciccarelli_ocean-waves.wav"
biophony = "../../data/audio/123999_nightvoice_forest-birds.wav"
anthropophony = "../../data/audio/240233_habbis92_piccadilly-circus-ambience.wav"
psd_plot_path = "../../output/plots/psd.png"
pop_plot_path = "../../output/plots/pop.png"

dt = 0.033 # our sample rate
steps = 885 # total samples to take (number of data points in Adria_Afraid_v1.csv)
growths = [0.01, 0.1, 0.5] # intrinsic growth rates (arbitrarily assigned)
interactions = np.array([
    [0, -0.04, -0.04],
    [0.04, 0, -0.02],
    [0.02, 0.04, -0.1]
])

def main():
    areas = psd(geophony, biophony, anthropophony, psd_plot_path)
    c0 = initc(areas)
    popmodel = pop(c0, growths, interactions)
    popdynamics = popmodel.simulate(steps, dt)
    normpopdynamics = normalize(popdynamics, axis=0, norm='l1')
    popmodel.plotSimulation(normpopdynamics,dt, pop_plot_path)

if __name__ == "__main__":
    main()
