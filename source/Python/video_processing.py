from utils.optical_flow_bkgrd_subtract import opflowminus
from utils.frames_to_video import frames2vid

input_video_path = '../../data/video/Andria_Afraid_v1.mp4'
output_frames_path = '../../output/video_frames/Andria_Afraid_v1_opt_flow_bkgrd_sub'
input_frames_path = '../../output/video_frames/Andria_Afraid_v1_opt_flow_bkgrd_sub'
output_video_path = '../../output/video_render/Andria_Afraid_v1_opt_flow_bkgrd_sub.avi'
fps = 30.0

def main():
    opflowminus(input_video_path, output_frames_path)
    frames2vid(input_frames_path, output_video_path, fps)

if __name__ == "__main__":
    main()
