import cv2
import numpy as np
import os
import re
from os.path import isfile, join

def frames2vid(input_frames_path, output_video_path,fps):
    frame_array = []
    #for filename in glob.glob(args.input+'/*.jpeg'):
    files = [f for f in os.listdir(input_frames_path) if isfile(join(input_frames_path, f))]
    # sorting the file names properly
    files = sorted(files, key=lambda x:float(re.findall("(\d+)",x)[0]))
    for i in range(len(files)):
        filename=join(input_frames_path, files[i])
        #reading each file
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        #print(filename)
        # inserting the frames into an image array
        frame_array.append(img)
    out = cv2.VideoWriter(output_video_path,cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
    for i in range(len(frame_array)):
        # writing to a image array
        out.write(frame_array[i])
    out.release()
